import asyncore
from smtpd import SMTPServer
import socketio
import email
from datetime import datetime

sio = None

class EmlServer(SMTPServer):
    no = 0
    def process_message(self, peer, mailfrom, rcpttos, data, mail_options=None,rcpt_options=None):
        global sio
        if sio is None:
            sio = socketio.Client()
            sio.connect('http://stack-manager.planblick.svc:5000/')

        b = email.message_from_string(data.decode())
        body = ""

        filename = '/src/app/%s-%d.eml' % (datetime.now().strftime('%Y%m%d%H%M%S'), self.no)
        self.no += 1
        f = open(filename, 'wb')
        f.write(data)
        f.close
        print('%s saved.' % filename)

        if b.is_multipart():
            for part in b.walk():
                ctype = part.get_content_type()
                cdispo = str(part.get('Content-Disposition'))

                # skip any text/plain (txt) attachments
                if ctype == 'text/plain' and 'attachment' not in cdispo:
                    body = part.get_payload(decode=True)  # decode
                    break
        # not multipart - i.e. plain text, no attachments, keeping fingers crossed
        else:
            body = b.get_payload(decode=True)

        print("GOT MAIL:", body.decode())
        #sio.emit('addLog', {"message": "Email received: <pre>" + body.decode() + "</pre>"})

def run():
    # start the smtp server on localhost:1025
    foo = EmlServer(('0.0.0.0', 10025), None)
    try:
        asyncore.loop()
    except KeyboardInterrupt:
        pass

if __name__ == '__main__':
    run()